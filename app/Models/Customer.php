<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * Database table name
     * 
     * @var string
     */
    protected $table = 'customers';

    /**
     * Fillable properties
     * 
     * @var array
     */
    protected $fillable = ['team_id', 'name', 'document', 'status'];

    /**
     * Customer numbers
     *
     * @return HasMany
     */
    public function numbers(): HasMany
    {
        return $this->hasMany(Number::class);
    }
}
