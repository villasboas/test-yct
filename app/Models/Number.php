<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Number extends Model
{
    use SoftDeletes;
    use HasFactory;

    /**
     * Database table name
     * 
     * @var string
     */
    protected $table = 'numbers';

    /**
     * Fillable properties
     * 
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'number',
        'status'
    ];

    /**
     * Number owner
     * 
     * @return BelongsTo
     */
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * Number preferences
     * 
     * @return HasMany
     */
    public function numberPreferences(): HasMany
    {
        return $this->hasMany(NumberPreference::class);
    }
}
