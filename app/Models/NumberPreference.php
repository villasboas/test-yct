<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class NumberPreference extends Model
{
    use SoftDeletes;
    use HasFactory;

    /**
     * Database table name
     * 
     * @var string
     */
    protected $table = 'number_preferences';

    /**
     * Fillable properties
     * 
     * @var array
     */
    protected $fillable = ['number_id', 'name', 'value'];

    /**
     * Number belongsTo
     * 
     * @return BelongsTo
     */
    public function number(): BelongsTo
    {
        return $this->belongsTo(Number::class);
    }
}
