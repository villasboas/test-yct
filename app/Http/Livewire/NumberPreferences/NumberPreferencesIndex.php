<?php

namespace App\Http\Livewire\NumberPreferences;

use App\Actions\NumberPreferences\ViewAnyNumberPreference;
use App\Actions\NumberPreferences\DeleteNumberPreference;
use App\Actions\NumberPreferences\SaveNumberPreference;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Throwable;

class NumberPreferencesIndex extends Component
{
    use LivewireAlert;

    /**
     * Current number preferences
     * 
     * @param array
     */
    public $preferences = [];

    /**
     * Current number id
     *
     * @param string
     */
    public string $numberId;

    /**
     * Validation attributes rules
     *
     * @param array
     */
    protected $rules = [
        'preferences.*.name' => ['required', 'string', 'max:255'],
        'preferences.*.value' => ['required', 'string', 'max:255'],
    ];

    /**
     * Validation attributes messages
     *
     * @param array
     */
    protected $validationAttributes = [
        'preferences.*.name' => 'preference name',
        'preferences.*.value' => 'preference value',
    ];

    /**
     * Handle data initialization
     * 
     * @param viewNumberPreference $creator
     */
    public function mount(ViewAnyNumberPreference $creator)
    {
        $this->preferences = $creator(
            numberId: $this->numberId,
            user: Auth::user()
        )->toArray();
    }

    /**
     * Handle delete number
     * 
     * @param DeleteNumberPreference $creator
     * @param int $index
     */
    public function handleDelete(DeleteNumberPreference $creator, int $index)
    {
        try {
            $numberPreference = $this->preferences[$index];

            if ($numberPreference['id']) {
                $creator(user: Auth::user(), id: $numberPreference['id']);
            }

            unset($this->preferences[$index]);

            $this->alert('success', __('Number preference was successfuly removed'), [
                'position' => 'bottom-end'
            ]);
        } catch (Throwable $e) {
            $this->alert('error', __('Something went wrong. Please, check your information and try it again.'), [
                'position' => 'bottom-end'
            ]);
        }
    }

    /**
     * Add a new preference to array
     *
     */
    public function handleAdd()
    {
        $this->preferences[] = [
            ...$this->preferences,
            'id' => null,
            'number_id' => $this->numberId
        ];
    }

    /**
     * Save number preference
     * 
     * @param SaveNumberPrefenrece $creator
     * @param int $index
     */
    public function handleSave(SaveNumberPreference $creator, int $index)
    {
        $this->validate();
        try {

            $data = $this->preferences[$index];

            $preference = $creator(user: Auth::user(), data: $data, id: optional($data)['id']);

            $this->preferences[$index] = $preference;

            $this->alert('success', __('Number preference was successfuly updated'), [
                'position' => 'bottom-end'
            ]);
        } catch (Throwable $e) {
            $this->alert('error', __('Something went wrong. Please, check your information and try it again.'), [
                'position' => 'bottom-end'
            ]);
        }
    }

    /**
     * Render preferences list
     *
     */
    public function render()
    {
        return view('customers.components.number-preferences-index');
    }
}
