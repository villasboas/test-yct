<?php

namespace App\Http\Livewire\Numbers;

use Jantinnerezo\LivewireAlert\LivewireAlert;
use App\Actions\Numbers\ViewAnyNumber;
use App\Actions\Numbers\DeleteNumber;
use Illuminate\Support\Facades\Auth;
use App\Actions\Numbers\SaveNumber;
use Livewire\Component;
use Throwable;

class NumbersIndex extends Component
{
    use LivewireAlert;

    /**
     * Customer available numbers
     * 
     * @param array
     */
    public $numbers = [];

    /**
     * Customer id
     * 
     * @param string
     */
    public string | null $resourceId = null;

    /**
     * Validation attributes rules
     *
     * @param array
     */
    protected $rules = [
        'numbers.*.number' => ['required', 'string', 'max:255'],
        'numbers.*.status' => ['required', 'string', 'max:255'],
    ];

    /**
     * Validation attributes messages
     *
     * @param array
     */
    protected $validationAttributes = [
        'numbers.*.number' => 'number',
        'numbers.*.status' => 'status',
    ];

    /**
     * Load initial data
     * 
     * @param ViewAnyNumber $creator
     */
    public function mount(ViewAnyNumber $creator)
    {
        $this->resourceId = request()->route('customer_id');

        $numbers = $creator(user: Auth::user(), customerId: $this->resourceId)->toArray();

        if (count($numbers) === 0) {
            $this->handleNewNumber();
            return;
        }

        $this->numbers = $numbers;
    }

    /**
     * Add new number to array
     * 
     *
     */
    public function handleNewNumber()
    {
        $this->numbers = [
            ...$this->numbers,
            ['number_preferences' => [], 'number' => '', 'status' => 'active', 'customer_id' => '', 'id' => null]
        ];
    }

    /**
     * Handle number delete
     * 
     * @param DeleteNumber $creator
     * @param int $index
     */
    public function handleDelete(DeleteNumber $creator, $index)
    {
        try {
            $number = $this->numbers[$index];

            if ($number['id']) {
                $creator(user: Auth::user(), id: $number['id']);
            }

            unset($this->numbers[$index]);

            if (count($this->numbers) === 0) {
                $this->handleNewNumber();
            }

            $this->alert('success', __('Number was successfuly removed'), [
                'position' => 'bottom-end'
            ]);
        } catch (Throwable $e) {
            $this->alert('error', __('Something went wrong. Please, check your information and try it again.'), [
                'position' => 'bottom-end'
            ]);
        }
    }

    /**
     * Handle number saving
     * 
     * @param SaveNumber $creator
     * @param int $index
     */
    public function handleSave(SaveNumber $creator, $index)
    {
        $this->validate();
        try {

            $data = $this->numbers[$index];

            $data['customer_id'] = $this->resourceId;

            $number = $creator(user: Auth::user(), data: $data, id: $data['id']);

            $this->numbers[$index] = $number->load('numberPreferences')->toArray();

            $this->alert('success', __('Number was successfuly updated'), [
                'position' => 'bottom-end'
            ]);
        } catch (Throwable $e) {
            $this->alert('error', __('Something went wrong. Please, check your information and try it again.'), [
                'position' => 'bottom-end'
            ]);
        }
    }

    /**
     * Render numbers index
     *
     */
    public function render()
    {
        return view('customers.components.numbers-index');
    }
}
