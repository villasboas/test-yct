<?php

namespace App\Http\Livewire\Customers;

use Jantinnerezo\LivewireAlert\LivewireAlert;
use App\Actions\Customers\SavesCustomer;
use App\Actions\Customers\ViewCustomer;
use Laravel\Jetstream\RedirectsActions;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Throwable;

class CustomersForm extends Component
{
    use RedirectsActions;
    use LivewireAlert;

    /**
     * The component's state.
     *
     * @var array
     */
    public $state = [
        'status' => 'new'
    ];

    /**
     * Resource id
     * 
     * @var string
     */
    public $resourceId = null;

    /**
     * Validation rules
     * 
     * @var array
     */
    public $rules = [
        'state.name' => 'required',
        'state.document' => 'required',
        'state.status' => 'required'
    ];

    /**
     * Validation attributes messages
     *
     * @param array
     */
    protected $validationAttributes = [
        'state.name' => 'name',
        'state.value' => 'value',
    ];

    /**
     * Handle data initialization
     * 
     * @param ViewCustomer $creator
     */
    public function mount(ViewCustomer $creator)
    {
        $this->resourceId = request()->route('customer_id', null);
        if (!$this->resourceId) return;

        $customer = $creator(user: Auth::user(), id: $this->resourceId);

        $this->state = $customer->only('name', 'document', 'status');
    }

    /**
     * Create a new team.
     *
     * @param  \App\Actions\Customers\SavesCustomer  $creator
     * @return void
     */
    public function handleSubmit(SavesCustomer $creator)
    {
        $this->validate();

        try {
            $creator(
                user: Auth::user(),
                data: [
                    ...$this->state,
                    'team_id' => Auth::user()->currentTeam->id
                ],
                id: $this->resourceId
            );

            $this->alert('success', __('Customer was successfuly saved'), [
                'position' => 'bottom-end'
            ]);

            return $this->redirectPath($creator);
        } catch (Throwable $e) {
            $this->alert('error', __('Something went wrong. Please, check your information and try it again.'), [
                'position' => 'bottom-end'
            ]);
        }
    }

    /**
     * Render customers form component
     *
     */
    public function render()
    {
        return view('customers.components.customers-form');
    }
}
