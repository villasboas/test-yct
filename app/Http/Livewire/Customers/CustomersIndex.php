<?php

namespace App\Http\Livewire\Customers;

use Jantinnerezo\LivewireAlert\LivewireAlert;
use App\Actions\Customers\ViewAnyCustomer;
use App\Actions\Customers\DeleteCustomer;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Throwable;

class CustomersIndex extends Component
{
    use LivewireAlert;

    /**
     * The component's state.
     *
     * @var array
     */
    public $customers = [];

    /**
     * Handle customer remove
     * 
     * @param DeleteCustomer $creator
     * @param int $id
     */
    public function handleDelete(DeleteCustomer $creator, $index)
    {
        try {
            $customer = $this->customers[$index];

            $creator(user: Auth::user(), id: $customer->id);

            unset($this->customers[$index]);

            $this->alert('success', __('Customer was successfuly removed'), [
                'position' => 'bottom-end'
            ]);
        } catch (Throwable $e) {
            $this->alert('error', __('Something went wrong. Please, check your information and try it again.'), [
                'position' => 'bottom-end'
            ]);
        }
    }

    /**
     * Load initial data
     *
     */
    public function mount(ViewAnyCustomer $creator)
    {
        $this->customers = $creator(user: Auth::user());
    }

    /**
     * Render customer view
     *
     */
    public function render()
    {
        return view('customers.components.customers-index');
    }
}
