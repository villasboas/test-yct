<?php

namespace App\Actions\Customers;

use App\Models\Customer;
use Illuminate\Support\Facades\Gate;

class DeleteCustomer
{
    /**
     * Delete a customer from database
     * 
     * @param User $user
     * @param int $id
     */
    public function __invoke($user, $id)
    {
        Gate::forUser($user)->authorize('delete', Customer::class);

        return Customer::findOrFail($id)->delete();
    }
}
