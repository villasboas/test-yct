<?php

namespace App\Actions\Customers;

use App\Models\Customer;
use Illuminate\Support\Facades\Gate;

class ViewCustomer
{

    /**
     * Retrive customer by id
     * 
     * @param User $user
     * @param int $id
     */
    public function __invoke($user, $id)
    {
        Gate::forUser($user)->authorize('view', Customer::class);

        return Customer::findOrFail($id);
    }
}
