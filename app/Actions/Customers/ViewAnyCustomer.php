<?php

namespace App\Actions\Customers;

use App\Models\Customer;
use Illuminate\Support\Facades\Gate;

class ViewAnyCustomer
{

    /**
     * List all customer from team
     * 
     * @param User $user
     */
    public function __invoke($user)
    {
        Gate::forUser($user)->authorize('viewAny', Customer::class);

        return Customer::whereTeamId($user->currentTeam->id)->get();
    }
}
