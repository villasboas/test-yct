<?php

namespace App\Actions\Customers;

use App\Models\Customer;
use Illuminate\Support\Facades\Gate;

class SavesCustomer
{
    /**
     * Indicates page redirection
     * 
     * @var string
     */
    public $redirectTo = '';

    public function handleUpdate($id, $data)
    {
        $customer = Customer::findOrFail($id);

        $customer->fill($data)->save();

        $this->redirectTo = null;

        return $customer;
    }

    public function handleCreate($data)
    {

        $customer = Customer::create($data);

        $this->redirectTo = "customers/$customer->id/edit";

        return $customer;
    }

    /**
     * Saves  a customer
     * 
     */
    public function __invoke($user, $data, $id = null)
    {
        if ($id) {
            Gate::forUser($user)->authorize('update', Customer::class);

            return $this->handleUpdate(data: $data, id: $id);
        }

        Gate::forUser($user)->authorize('create', Customer::class);

        return $this->handleCreate($data);
    }
}
