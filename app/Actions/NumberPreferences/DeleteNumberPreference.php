<?php

namespace App\Actions\NumberPreferences;

use App\Models\NumberPreference;
use Illuminate\Support\Facades\Gate;

class DeleteNumberPreference
{
    /**
     * Delete a number preference
     * 
     * @param User $user
     * @param int $id
     */
    public function __invoke($user, $id)
    {
        Gate::forUser($user)->authorize('delete', NumberPreference::class);

        return NumberPreference::findOrFail($id)->delete();
    }
}
