<?php

namespace App\Actions\NumberPreferences;

use App\Models\NumberPreference;
use Illuminate\Support\Facades\Gate;

class ViewAnyNumberPreference
{
    /**
     * List number preferences
     * 
     * @param User $user
     * @param int $numberId
     */
    public function __invoke($user, $numberId)
    {
        Gate::forUser($user)->authorize('viewAny', NumberPreference::class);

        return NumberPreference::whereNumberId($numberId)->get();
    }
}
