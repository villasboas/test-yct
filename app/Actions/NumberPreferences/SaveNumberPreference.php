<?php

namespace App\Actions\NumberPreferences;

use App\Models\NumberPreference;
use Illuminate\Support\Facades\Gate;

class SaveNumberPreference
{
    /**
     * Handle number preference update
     * 
     * @param array $data
     * @param int $id
     */
    public function handleUpdate($data, $id)
    {
        $numberPreference = NumberPreference::findOrFail($id);

        $numberPreference->fill($data)->save();

        return $numberPreference;
    }

    /**
     * Handle number preference create
     * 
     * @param array $data     */
    public function handleCreate($data)
    {
        return NumberPreference::create($data);
    }

    public function __invoke(
        $user,
        $data,
        $id = null
    ) {
        if ($id) {
            Gate::forUser($user)->authorize('update', NumberPreference::class);

            return $this->handleUpdate($data, $id);
        }

        Gate::forUser($user)->authorize('create', NumberPreference::class);
        return $this->handleCreate($data);
    }
}
