<?php

namespace App\Actions\Numbers;

use App\Models\Number;
use Illuminate\Support\Facades\Gate;

class ViewAnyNumber
{
    /**
     * List all team numbers
     * 
     * @param User $user
     * @param int $customerId
     */
    public function __invoke($user, $customerId)
    {
        Gate::forUser($user)->authorize('viewAny', Number::class);

        return Number::whereCustomerId($customerId)->with('numberPreferences')->get();
    }
}
