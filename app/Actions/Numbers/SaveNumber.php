<?php

namespace App\Actions\Numbers;

use Illuminate\Support\Facades\DB;
use App\Models\NumberPreference;
use App\Models\Number;
use Illuminate\Support\Facades\Gate;

class SaveNumber
{
    /**
     * Handle record update
     * 
     * @param array $data
     * @param int $id
     */
    public function handleUpdate($data, $id)
    {
        $number = Number::findOrFail($id);

        $number->fill($data)->save();

        return $number;
    }

    /**
     * Handle record creation
     * 
     * @param array $data
     */
    public function handleCreate($data)
    {
        return DB::transaction(function () use ($data) {
            $number = Number::create($data);

            NumberPreference::insert([
                [
                    'number_id' => $number->id,
                    'name' => 'auto_attendant',
                    'value' => 1
                ],
                [
                    'number_id' => $number->id,
                    'name' => 'voicemail_enabled',
                    'value' => 1
                ]
            ]);

            return $number;
        });
    }

    /**
     * Handle number save
     * 
     * @param User $user
     * @param array $data
     * @param int $id
     */
    public function __invoke($user, $data, $id)
    {
        if ($id) {
            Gate::forUser($user)->authorize('update', Number::class);

            return $this->handleUpdate(
                data: $data,
                id: $id
            );
        }

        Gate::forUser($user)->authorize('create', Number::class);

        return $this->handleCreate($data);
    }
}
