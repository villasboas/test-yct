<?php

namespace App\Actions\Numbers;

use App\Models\Number;
use Illuminate\Support\Facades\Gate;

class DeleteNumber
{
    /**
     * Remove a number
     * 
     * @param User $user
     * @param int $id
     */
    public function __invoke($user, $id)
    {
        Gate::forUser($user)->authorize('delete', Number::class);

        return Number::findOrFail($id)->delete();
    }
}
