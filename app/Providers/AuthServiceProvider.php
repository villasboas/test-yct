<?php

namespace App\Providers;

use App\Models\Customer;
use App\Models\Number;
use App\Models\NumberPreference;
use App\Models\Team;
use App\Policies\CustomerPolicy;
use App\Policies\NumberPolicy;
use App\Policies\NumberPreferencePolicy;
use App\Policies\TeamPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Team::class => TeamPolicy::class,
        Customer::class => CustomerPolicy::class,
        Number::class => NumberPolicy::class,
        NumberPreference::class => NumberPreferencePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
