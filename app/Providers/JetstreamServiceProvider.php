<?php

namespace App\Providers;

use App\Actions\Jetstream\AddTeamMember;
use App\Actions\Jetstream\CreateTeam;
use App\Actions\Jetstream\DeleteTeam;
use App\Actions\Jetstream\DeleteUser;
use App\Actions\Jetstream\InviteTeamMember;
use App\Actions\Jetstream\RemoveTeamMember;
use App\Actions\Jetstream\UpdateTeamName;
use Illuminate\Support\ServiceProvider;
use Laravel\Jetstream\Jetstream;

class JetstreamServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configurePermissions();

        Jetstream::createTeamsUsing(CreateTeam::class);
        Jetstream::updateTeamNamesUsing(UpdateTeamName::class);
        Jetstream::addTeamMembersUsing(AddTeamMember::class);
        Jetstream::inviteTeamMembersUsing(InviteTeamMember::class);
        Jetstream::removeTeamMembersUsing(RemoveTeamMember::class);
        Jetstream::deleteTeamsUsing(DeleteTeam::class);
        Jetstream::deleteUsersUsing(DeleteUser::class);
    }

    /**
     * Configure the roles and permissions that are available within the application.
     *
     * @return void
     */
    protected function configurePermissions()
    {
        Jetstream::defaultApiTokenPermissions(['read']);

        Jetstream::role('admin', 'Administrator', [
            'customer:viewAny',
            'customer:view',
            'customer:create',
            'customer:read',
            'customer:update',
            'customer:delete',

            'number:viewAny',
            'number:view',
            'number:create',
            'number:read',
            'number:update',
            'number:delete',

            'numberPreferences:viewAny',
            'numberPreferences:view',
            'numberPreferences:create',
            'numberPreferences:read',
            'numberPreferences:update',
            'numberPreferences:delete',
        ])->description('Administrator users can perform any action.');

        Jetstream::role('reader', 'Reader', [
            'customer:viewAny',
            'customer:view',

            'number:viewAny',
            'number:view',


            'numberPreferences:viewAny',
            'numberPreferences:view',
        ])->description('Editor users have the ability to read, create, and update.');
    }
}
