# Test YCT

## Demo:

A demo is available on:

test-yct.villasboas.dev

https://www.loom.com/share/11c19ff3d2394a9c88cb32fe185b4166

## Features

In this project, the current features were implanted:

-   [x] Create and edit an account
-   [x] Create, edit, remove teams and teams members
-   [x] Team level permissions
-   [x] Create, edit, display customers
-   [x] Create, edit, display numbers
-   [x] Create, edit, display number preferences
-   [x] 100% support for translation, using Laravel between tools
-   [x] Usage of Eloquent ORM

## Getting Started

### Technolgies

In this projetct, were used the following technologies:

-   [x] Laravel Jetstream: for boilerplate, authentication and authorization, team management.
-   [x] Laravel Sail: for a development environment with: mysql, php and mailhog
-   [x] Livewire: for fluid interaction between frontend and backend

### Install

#### Requisites

-   [x] Laravel sail
-   [x] Docker

#### Steps

1 - Clone this repository.
2 - Open the terminal, in the root folder, and run:

```bash
sail up -d
sail composer install
sail artisan migrate
```

3 - If you are under development mode, you should also run the following commands to
compile frontend assets:

```bash
yarn
yarn watch
```

### Contact

For questions, you may contact me on:

gu.boas13@gmail.com
+55 51 99948-0446
