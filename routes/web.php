<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/customers', function () {
    return view('customers.index');
})->name('customers.index');

Route::middleware(['auth:sanctum', 'verified'])->get('/customers/create', function () {
    return view('customers.form');
})->name('customers.create');

Route::middleware(['auth:sanctum', 'verified'])->get('/customers/{customer_id}/edit', function () {
    return view('customers.form');
})->name('customers.edit');