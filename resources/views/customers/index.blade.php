<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Customers') }}
        </h2>
    </x-slot>

    <div class="py-12">
        @can('create', \App\Models\Customer::class)
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 pb-1">
                <x-jet-button type="button" onclick="location.href = '{{ route('customers.create') }}'">
                    {{ __('Add customer') }}
                </x-jet-button>
            </div>
        @endcan

        <livewire:customers.customers-index />

    </div>
</x-app-layout>
