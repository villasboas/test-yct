<x-jet-form-section submit="handleSubmit">
    <x-slot name="title">
        {{ __('Customer Details') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Customer basic information') }}
    </x-slot>

    <x-slot name="form">
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="state.name" value="{{ __('Customer Name') }}" />
            <x-jet-input id="state.name" type="text" class="mt-1 block w-full" wire:model.defer="state.name"
                :disabled="auth()->user()->cannot(['create', 'update'], \App\Models\Customer::class)" />
            <x-jet-input-error for="state.name" class="mt-2" />
        </div>
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="state.document" value="{{ __('Customer Document') }}" />
            <x-jet-input :disabled="auth()->user()->cannot(['create', 'update'], \App\Models\Customer::class)"
                id="state.document" type="text" class="mt-1 block w-full" wire:model.defer="state.document" />
            <x-jet-input-error for="state.document" class="mt-2" />
        </div>
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="state.satus" value="{{ __('Status') }}" />
            <select @if(auth()->user()->cannot(['create', 'update'], \App\Models\Customer::class)) disabled="disabled" @endif
                wire:model="state.status" name="status" id="status"
                class="border-gray-300 w-full focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm">
                <option value="new">{{ __('New') }}</option>
                <option value="active">{{ __('Active') }}</option>
                <option value="suspended">{{ __('Suspended') }}</option>
                <option value="cancelled">{{ __('Cancelled') }}</option>
            </select>
            <x-jet-input-error for="state.satus" class="mt-2" />
        </div>
    </x-slot>

    @if(auth()->user()->can(['create', 'update'], \App\Models\Customer::class))
    <x-slot name="actions">
        <x-jet-button>
            {{ __('Save') }}
        </x-jet-button>
    </x-slot>
    @endif
</x-jet-form-section>
