 <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
     <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
         <table class="min-w-full divide-y divide-gray-200">
             <thead class="bg-gray-50">
                 <tr>
                     <th scope="col"
                         class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                         {{ __('Name') }}
                     </th>
                     <th scope="col"
                         class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                         {{ __('Document') }}
                     </th>
                     <th scope="col"
                         class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                         {{ __('Status') }}
                     </th>
                     <th scope="col" class="relative px-6 py-3">
                         <span class="sr-only">Edit</span>
                     </th>
                 </tr>
             </thead>
             <tbody class="bg-white divide-y divide-gray-200">

                 @foreach ($customers as $index => $customer)
                     <tr>
                         <td class="px-6 py-4 whitespace-nowrap">
                             <div class="flex items-center">

                                 <div class="ml-4">
                                     <div class="text-sm font-medium text-gray-900">
                                         {{ $customer->name }}
                                     </div>
                                 </div>
                             </div>
                         </td>
                         <td class="px-6 py-4 whitespace-nowrap">
                             <div class="text-sm text-gray-900">
                                 {{ $customer->document }}
                             </div>
                         </td>
                         <td class="px-6 py-4 whitespace-nowrap">
                             <div class="text-sm text-gray-900">
                                 {{ $customer->status }}
                             </div>
                         </td>
                         <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                             @can('view', $customer)
                                 <x-jet-button type="button"
                                     onclick="location.href = '{{ route('customers.edit', [
                                         'customer_id' => $customer->id,
                                     ]) }}'">
                                     {{ __('Show') }}
                                 </x-jet-button>
                             @endcan
                             @can('delete', $customer)
                                 <x-jet-danger-button wire:click="handleDelete({{ $index }})">
                                     {{ __('Remove') }}
                                 </x-jet-danger-button>
                             @endcan
                         </td>
                     </tr>
                 @endforeach

             </tbody>
         </table>
     </div>
 </div>
