<div class="mt-3 md:grid md:grid-cols-3 md:gap-6">
    <x-jet-section-title>
        <x-slot name="title">{{ 'Numbers' }}</x-slot>
        <x-slot name="description">{{ 'Customer available numbers' }}</x-slot>
    </x-jet-section-title>


    <div class="mt-5 md:mt-0 md:col-span-2">
        @foreach ($numbers as $index => $number)
            <div class="bg-white px-4 py-5 mt-5 sm:p-6 shadow sm:rounded-tl-md sm:rounded-tr-md">
                <table class="w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50">
                        <tr>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                {{ __('Number') }}
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                {{ __('Status') }}
                            </th>
                            <th scope="col" class="relative px-6 py-3 text-right">
                                @if (auth()->user()->can(['create'], \App\Models\Number::class))
                                    <a wire:click.prevent="handleNewNumber"
                                        class="text-indigo-600 hover:text-indigo-900">
                                        {{ __('+ Add number') }}
                                    </a>
                                @endif
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                        <tr>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <x-jet-input id="numbers.{{ $index }}.number" type="tel"
                                    placeholder="{{ __('Phone number') }}" class="w-full"
                                    :disabled="auth()->user()->cannot(['create', 'update'], \App\Models\Number::class)"
                                    wire:model.defer="numbers.{{ $index }}.number" />
                                <x-jet-input-error for="numbers.{{ $index }}.number" class="mt-2" />
                            </td>
                            <td colspan="2" class="px-6 py-4 whitespace-nowrap">
                                <select wire:model="numbers.{{ $index }}.status" @if (auth()->user()->cannot(['create', 'update'], \App\Models\Number::class)) disabled="disabled" @endif
                                    name="numbers.{{ $index }}.status" id="numbers.{{ $index }}.status"
                                    class="border-gray-300 w-full focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm">
                                    <option value="active">{{ __('Active') }}</option>
                                    <option value="inactive">{{ __('Inactive') }}</option>
                                    <option value="cancelled">{{ __('Cancelled') }}</option>
                                </select>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                @if (auth()->user()->can(['delete'], \App\Models\Number::class))
                                    <x-jet-danger-button wire:click="handleDelete({{ $index }})">
                                        {{ __('Remove') }}
                                    </x-jet-danger-button>
                                @endif
                                @if (auth()->user()->can(['create', 'update'], \App\Models\Number::class))
                                    <x-jet-button type="button" wire:click="handleSave({{ $index }})">
                                        {{ __('Save') }}
                                    </x-jet-button>
                                @endif
                            </td>
                        </tr>

                        @if ($number['id'])
                            <tr>
                                <td colspan="4">
                                    @livewire('number-preferences.number-preferences-index', [
                                    'numberId' => $number['id'],
                                    ], key($index))
                                </td>
                            </tr>

                        @endif
                    </tbody>
                </table>

            </div>

        @endforeach
    </div>
</div>
