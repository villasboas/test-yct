<table class="w-full divide-y divide-gray-200">
    <thead class="bg-gray-50">
        <tr>
            <th scope="col" colspan="2"
                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                {{ __('Number Preferences') }}
            </th>
            <th scope="col" class="relative px-6 py-3 text-right">
                @if (auth()->user()->can(['create'], \App\Models\NumberPreference::class))
                    <a wire:click.prevent="handleAdd" class="text-indigo-600 hover:text-indigo-900">
                        {{ __('+ New') }}
                    </a>
                @endif
            </th>
        </tr>
    </thead>
    <tbody class="bg-white divide-y divide-gray-200">
        @foreach ($preferences as $index => $preference)
            <tr>
                <td class="px-6 py-4 whitespace-nowrap">
                    <x-jet-input id="preferences.{{ $index }}.name" type="text" class="mt-1 block w-full"
                        placeholder="{{ __('Preference name') }}"
                        :disabled="auth()->user()->cannot(['create', 'update'], \App\Models\NumberPreference::class)"
                        wire:model.defer="preferences.{{ $index }}.name" />
                    <x-jet-input-error for="preferences.{{ $index }}.name" class="mt-2" />
                </td>
                <td class="px-6 py-4 whitespace-nowrap">
                    <x-jet-input id="preferences.{{ $index }}.value" type="text" class="mt-1 block w-full"
                        placeholder="{{ __('Preference value') }}"
                        :disabled="auth()->user()->cannot(['create', 'update'], \App\Models\NumberPreference::class)"
                        wire:model.defer="preferences.{{ $index }}.value" />
                    <x-jet-input-error for="preferences.{{ $index }}.value" class="mt-2" />
                </td>
                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                    @if (auth()->user()->can(['delete'], \App\Models\NumberPreference::class))

                        <x-jet-danger-button class="mr-1" wire:click="handleDelete({{ $index }})">
                            {{ __('Remove') }}
                        </x-jet-danger-button>
                    @endif
                    @if (auth()->user()->can(['update', 'create'], \App\Models\NumberPreference::class))
                        <x-jet-button type="button" wire:loading.attr="disabled"
                            wire:click="handleSave({{ $index }})">
                            {{ __('Save') }}
                        </x-jet-button>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
