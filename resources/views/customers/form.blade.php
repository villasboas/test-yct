<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Customers') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div x-data="{ numberId: false }" class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">

            <livewire:customers.customers-form />

            @if (request()->routeIs('customers.edit'))
                <livewire:numbers.numbers-index />
            @endif
        </div>
    </div>
</x-app-layout>
